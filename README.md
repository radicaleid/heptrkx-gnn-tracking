# Graph Neural Networks for particle track reconstruction

This repository contains the PyTorch implementation of the GNNs for particle
track reconstruction from CTD 2018: https://arxiv.org/abs/1810.06111.

## Contents

The main python scripts for running:
- *[prepare.py](prepare.py)*: the data preparation script which reads
TrackML data files, cleans and reduces the data, and writes hit graphs to
the filesystem.
- *[train.py](train.py)*: the main training script which is steered by
configuration file and loads the data, model, and trainer, and invokes
the trainer to train the model.

Other stuff:
- In the scripts directory are SLURM batch scripts for running the jobs
on Cori at NERSC.
- The GNN model code lives in [models/gnn.py](models/gnn.py).
- The dataset code for reading the prepared hit graphs lives in
[datasets/hitgraphs.py](datasets/hitgraphs.py).
- The main trainer code for the GNN segment classifier lives in
[trainers/gnn.py](trainers/gnn.py).

# trackML package
git clone https://github.com/LAL/trackml-library.git

# build pytorch with mpi
https://github.com/pytorch/pytorch#from-source

# jupyter notebook on LCRC
ssh -N -f -L localhost:8888:localhost:8889 heplogin2.lcrc.anl.gov
jupyter notebook  --notebook-dir /lcrc/group/ATLAS/users/rwang --no-browser --port=8889

## For atlasMLbtagging data
- convert root ntuple to cvs for each event
cd dataset
python root_to_csv.py -i ../../dataset/test.root -o ../../output -p 1

- prepare input for training
python prepare_alcf.py configs/prep_alcf.yaml -v --n-workers 6

- train
python train.py
