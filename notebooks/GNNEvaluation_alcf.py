#!/usr/bin/env python
# coding: utf-8

# # Evaluation of Graph Neural Network segment classifier
# Concurrency config
import os
os.environ['OMP_NUM_THREADS'] = '4'
import yaml
import numpy as np
import sklearn.metrics

import torch
from torch.utils.data import Subset, DataLoader

import matplotlib.pyplot as plt

# Local software setup
import sys
sys.path.append('..')

from models import get_model
from datasets.hitgraphs import HitGraphDataset, collate_fn

#get_ipython().run_line_magic('matplotlib', 'inline')

# Loading
def get_output_dir(config):
    return os.path.expandvars(config['output_dir'])

def get_input_dir(config):
    return os.path.expandvars(config['data']['input_dir'])

def load_config(config_file):
    with open(config_file) as f:
        return yaml.load(f)

def load_summaries(config):
    summary_file = os.path.join(get_output_dir(config), 'summaries.npz')
    return np.load(summary_file)

def load_model(config, reload_epoch):
    print('loading model')
    model_config = config['model']
    model_type = model_config.pop('name')
    model_config.pop('optimizer', None)
    model_config.pop('learning_rate', None)
    model_config.pop('loss_func', None)
    model = get_model(name=model_type, **model_config)

    # Reload specified model checkpoint
    output_dir = get_output_dir(config)
    checkpoint_file = os.path.join(output_dir, 'checkpoints',
                                   'model_checkpoint_%03i.pth.tar' % reload_epoch)
    state_dict = torch.load(checkpoint_file,map_location='cpu')['model']
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
      name = k.replace('module.', '')  # remove 'module.' of dataparallel
      new_state_dict[name]=v
    model.load_state_dict(new_state_dict)
    return model

# Config path
config_file = '../configs/segclf_alcf.yaml'

#get_ipython().system('cat $config_file')

config = load_config(config_file)
summaries = load_summaries(config)

# ## Plot the Training loss and accuracy
fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(10, 5))

ax0.plot(summaries['epoch'], summaries['train_loss'], label='Train')
ax0.plot(summaries['epoch'], summaries['valid_loss'], label='Validation')
ax0.set_xlabel('Epoch', )
ax0.set_ylabel('Loss')
ax0.legend(loc=0)

ax1.plot(summaries['epoch'], summaries['train_acc'], label='Train')
ax1.plot(summaries['epoch'], summaries['valid_acc'], label='Validation')
ax1.set_xlabel('Epoch')
ax1.set_ylabel('Accuracy')
ax1.set_ylim(bottom=0, )
ax1.legend(loc=0)

plt.tight_layout()
plt.savefig('images/summaries.png')

# ## Load the trained model
model = load_model(config, reload_epoch=31).eval()

# ## Load the test data set
n_test = 128

full_dataset = HitGraphDataset(get_input_dir(config))

# Take the test set from the back
test_indices = len(full_dataset) - 1 - torch.arange(n_test)
test_dataset = Subset(full_dataset, test_indices)

# Construct a data loaer
test_loader = DataLoader(test_dataset, collate_fn=collate_fn)

# ## Evaluate the model on the test dataset
global test_pred, test_target,test_outputs
with torch.no_grad():
  test_outputs = [(model(batch_input).flatten(), batch_target.flatten())
                  for (batch_input, batch_target) in test_loader]
  test_pred, test_target = zip(*test_outputs)
  test_pred = np.concatenate(test_pred)
  test_target = np.concatenate(test_target)

thresh = 0.5
y_pred, y_true = (test_pred > thresh), (test_target > thresh)
print('Test set results with threshold of', thresh)
print('Accuracy:  %.4f' % sklearn.metrics.accuracy_score(y_true, y_pred))
print('Precision: %.4f' % sklearn.metrics.precision_score(y_true, y_pred))
print('Recall:    %.4f' % sklearn.metrics.recall_score(y_true, y_pred))

# ROC curve
fpr, tpr, _ = sklearn.metrics.roc_curve(y_true, test_pred)

fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(12,5))

# Plot the model outputs
binning=dict(bins=50, range=(0,1), histtype='bar', log=True)
ax0.hist(test_pred[y_true==False], label='fake', **binning)
ax0.hist(test_pred[y_true], label='true', **binning)
ax0.set_xlabel('Model output')
ax0.legend(loc=0)

# Plot the ROC curve
auc = sklearn.metrics.auc(fpr, tpr)
ax1.plot(fpr, tpr)
ax1.plot([0, 1], [0, 1], '--')
ax1.set_xlabel('False positive rate')
ax1.set_ylabel('True positive rate')
ax1.set_title('ROC curve, AUC = %.3f' % auc)

plt.tight_layout()
plt.savefig('images/ROC.png')


# ## Visualize predictions
def draw_sample(X, Ri, Ro, y, pred,threshold, i, cmap='bwr_r', alpha_labels=True, figsize=(14, 12)):
    # Select the i/o node features for each segment
    feats_o = X[np.where(Ri.T)[1]]
    feats_i = X[np.where(Ro.T)[1]]
    # Prepare the figure
    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=figsize)
    cmap = plt.get_cmap(cmap)

    # Draw the hits (r, phi, z)
    ax0.scatter(X[:,2], X[:,0], c='k')
    ax1.scatter(X[:,1], X[:,0], c='k')
    ax2.scatter(X[:,2], X[:,0], c='k')
    ax3.scatter(X[:,1], X[:,0], c='k')

    n_true=0
    n_pred=0
    n_matched=0
    # Draw the segments
    for j in range(y.shape[0]):
      if y[j]:
        seg_args = dict(c='r')
        ax2.plot([feats_o[j,2], feats_i[j,2]],
                 [feats_o[j,0], feats_i[j,0]], '-', **seg_args)
        ax3.plot([feats_o[j,1], feats_i[j,1]],
                 [feats_o[j,0], feats_i[j,0]], '-', **seg_args)
        n_true+=1

    # Draw the segments
    for j in range(pred.shape[0]):
      if pred[j]>threshold:
        if alpha_labels:
            seg_args = dict(c='k', alpha=float(pred[j]))
        else:
            seg_args = dict(c=cmap(float(pred[j])))
        ax0.plot([feats_o[j,2], feats_i[j,2]],
                 [feats_o[j,0], feats_i[j,0]], '-', **seg_args)
        ax1.plot([feats_o[j,1], feats_i[j,1]],
                 [feats_o[j,0], feats_i[j,0]], '-', **seg_args)
        n_pred+=1
        if y[j]>0:n_matched+=1
    #stat
    ax0.text(-0.7, 0.15, 'pred: %d'%n_pred)
    ax0.text(-0.7, 0.13, 'matched: %d'%n_matched)
    ax2.text(-0.7, 0.15, 'true: %d'%n_true)


    # Adjust axes
    ax0.set_xlabel('$z$')
    ax1.set_xlabel('$\phi$')
    ax2.set_xlabel('$z$')
    ax3.set_xlabel('$\phi$')
    ax0.set_ylabel('$r$')
    ax1.set_ylabel('$r$')
    ax2.set_ylabel('$r$')
    ax3.set_ylabel('$r$')
    plt.tight_layout()
    plt.savefig('images/event%d.png'%i)

n_draw = 4
for i in range(n_draw):
  X, Ri, Ro, y = test_dataset[i]
  pred = test_outputs[i][0].numpy()
  draw_sample(X, Ri, Ro, y, pred,thresh, i, alpha_labels=False)
