import os
import sys,glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# ### Get hits from pre event csv file
def select_hits(filelist,pt_min=0):
    all_hits=pd.DataFrame()
    for file in filelist:
        hits = pd.read_csv(file, sep='\t', index_col=False,header=0,
                           dtype={'hit_id':np.ulonglong,'r':np.float16,'phi':np.float16,'z':np.float16,
                                  'volume_id':np.int8,'layer_id':np.int8,'particle_id':np.int32,
                                  'particle_pt':np.half})
        evtid=int(file.split('_')[-1].split('.')[0])
        hits['evtid']=pd.Series(np.full(len(hits),evtid), index=hits.index)
        vlids = [(8,2), (8,4), (8,6), (8,8),(13,2), (13,4), (13,6), (13,8)]
        n_det_layers = len(vlids)
        # Select barrel layers and assign convenient layer number [0-9]
        vlid_groups = hits.groupby(['volume_id', 'layer_id'])
        hits = pd.concat([vlid_groups.get_group(vlids[i]).assign(layer=i)
                        for i in range(n_det_layers)])
        all_hits=all_hits.append(hits)
    return all_hits

filelist=[]
filelist.extend(glob.glob('../csvdata/u/*_1*.csv'))
#filelist.extend(glob.glob('../csvdata/u/*_2*.csv'))
print('%s files'%len(filelist))
hits=select_hits(filelist)

# ### Geometry ID pairs
# We use geometry IDs to select initial set of hit pair segments. For now we're starting with barrel hits only and can use the 0-9 layer number as the ID. We'll then use consecutive layer numbers as the criteria.
#
# This logic should be made extensible to non-barrel geometry and even module-level pairings.

gid_keys = 'layer'
n_det_layers = 8
gid_start = np.arange(0, n_det_layers-1)
gid_end = np.arange(1, n_det_layers)


def get_segments(hits, gid_keys, gid_start, gid_end):
    # Group hits by geometry ID
    hit_gid_groups = hits.groupby(gid_keys)

    segments = []

    # Loop over geometry ID pairs
    for gid1, gid2 in zip(gid_start, gid_end):
        hits1 = hit_gid_groups.get_group(gid1)
        hits2 = hit_gid_groups.get_group(gid2)

        # Join all hit pairs together
        hit_pairs = pd.merge(
            hits1.reset_index(), hits2.reset_index(),
            how='inner', on='evtid', suffixes=('_1', '_2'))

        # Calculate coordinate differences
        dphi = calc_dphi(hit_pairs.phi_1, hit_pairs.phi_2)
        dz = hit_pairs.z_2 - hit_pairs.z_1
        dr = hit_pairs.r_2 - hit_pairs.r_1
        phi_slope = dphi / dr
        z0 = hit_pairs.z_1 - hit_pairs.r_1 * dz / dr

        # Identify the true pairs
        y = (hit_pairs.particle_id_1 == hit_pairs.particle_id_2) & (hit_pairs.particle_id_1 != -1)

        # Put the results in a new dataframe
        segments.append(hit_pairs[['evtid', 'index_1', 'index_2', 'layer_1', 'layer_2']]
                        .assign(dphi=dphi, dz=dz, dr=dr, y=y, phi_slope=phi_slope, z0=z0, pt=hit_pairs.particle_pt_1))

    return pd.concat(segments, ignore_index=True)

def calc_dphi(phi1, phi2):
    """Computes phi2-phi1 given in range [-pi,pi]"""
    dphi = phi2 - phi1
    dphi[dphi > np.pi] -= 2*np.pi
    dphi[dphi < -np.pi] += 2*np.pi
    return dphi

segments = get_segments(hits=hits, gid_keys=gid_keys, gid_start=gid_start, gid_end=gid_end)
#segments.to_hdf('datasets/segments.h5', mode='w')
segments.head(2)


# ### Plot the full segment distributions

plt.figure(figsize=(14,6))

true_segs = segments[segments.y]
fake_segs = segments[segments.y == False]

plt.subplot(121)
binning=dict(bins=50, range=(-0.01, 0.01))
plt.hist(fake_segs.phi_slope, label='fake', log=True, **binning)
plt.hist(true_segs.phi_slope, label='true', **binning)
plt.xlabel('$\Delta \phi / \Delta r$ [rad/mm]')
plt.legend(loc=0)

plt.subplot(122)
binning=dict(bins=50, range=(-1000, 1000))
plt.hist(fake_segs.z0, label='fake', log=True, **binning)
plt.hist(true_segs.z0, label='true', **binning)
plt.xlabel('$z_0$ [mm]')
plt.legend(loc=0)

plt.tight_layout()
plt.savefig('images/segment_distribution.png')


# ### Segment selection
def select_segments(segments, phi_slope_min, phi_slope_max, z0_max):
    sel_mask = ((segments.phi_slope.abs() > phi_slope_min) &
                (segments.phi_slope.abs() < phi_slope_max) &
                (segments.z0.abs() < z0_max))
    return segments.assign(selected=sel_mask)

def segment_efficiency(segments):
    return (segments.y & segments.selected).sum() / segments.y.sum()

def segment_purity(segment):
    return (segments.y & segments.selected).sum() / segments.selected.sum()

# Choose some cuts
phi_slope_min = 0.
phi_slope_max = 0.004
z0_max = 200

segments = select_segments(segments, phi_slope_min=phi_slope_min,
                           phi_slope_max=phi_slope_max, z0_max=z0_max)
purity=segment_purity(segments)
print('Selection efficiency %.4f purity %.4f' % (segment_efficiency(segments), segment_purity(segments)))

# What is the selection efficiency for high pt particle segments?
true_segments = segments[segments.y]
#true_seg_pt = hits.loc[true_segments.index_1].particle_pt.values
#print(len(true_segments.index_1),len(true_segments),len(true_seg_pt))
#true_segments = true_segments.assign(pt=true_seg_pt)

print('Selection efficiency for high pt particles:')
print('  %.4f for pt > 500 MeV' % segment_efficiency(true_segments[true_segments.pt > 500]))
print('  %.4f for pt > 1.0 GeV' % segment_efficiency(true_segments[true_segments.pt > 1000]))


# ### Sample reweighting
#
# To deal with class imbalance, we'll want to reweight the segments in the training loss functions so that real and fake segments have nearly equal proportion.

# In[ ]:


# Approximating the sample fractions [fake, real] found above
sample_fracs = np.array([round(1-purity,1), round(purity,1)])

# Reweight each to 0.5
sample_weights = 0.5 / sample_fracs

print('sample_weights [fake, real]:', sample_weights)

############## l-jet ####################
# Selection efficiency 0.4655 purity 0.0637
# Selection efficiency for high pt particles:
  # 0.7055 for pt > 500 MeV
  # 0.7849 for pt > 1.0 GeV
# sample_weights [fake, real]: [0.55555556 5.        ]
