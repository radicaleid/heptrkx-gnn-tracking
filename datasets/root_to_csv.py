import os,optparse,logging,glob
import shutil
import multiprocessing
import numpy as np
import ROOT
#import matplotlib.pyplot as plt
#from matplotlib.pyplot import imshow

logger = logging.getLogger(__name__)
options = None

def main():
    global options
    logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s:%(process)s:%(name)s:%(message)s')

    parser = optparse.OptionParser(description='')
    parser.add_option('-i',dest='input_files',default="",help='glob input files, use quotes')
    parser.add_option('-o','--output-path',dest='output_path',default='./figures/2D/',help='path where to output data')
    parser.add_option('-p','--nprocs',dest='nprocs',type='int',default=1,help='set this value to choose how many subprocesses to spawn')
    options,args = parser.parse_args()

    logger.info('input:    %s' % options.input_files)
    logger.info('nprocs:   %s' % options.nprocs)
    logger.info('output    %s'%options.output_path)
    if os.path.exists(options.output_path):
      shutil.rmtree(options.output_path)
    os.makedirs(options.output_path)

    # get total number of files to process
    filelist = glob.glob(options.input_files)
    num_evts = len(filelist)
    evt_indices = range(num_evts)
    logger.info('processing %s events',num_evts)
    p = multiprocessing.Pool(options.nprocs)
    p.map(process_one_file,filelist)
    logger.info('done with all processes')

def process_one_file(file):
    f=ROOT.TFile.Open(file)
    tree=f.Get('sihits')
    for idx,event in enumerate(tree):
      with open(options.output_path+'/'+file.split('/')[-1].replace('.root','') + '_'+str(idx)+'.csv','a') as csvfile:
        line='hit_id\tr\tphi\tz\tlayer_id\tvolume_id\tparticle_id\tparticle_pt\n'
        csvfile.write(line)
        for i in range(event.PIX_rdoID.size()):
          if abs(event.PIX_barrelEndcap[i])==4: continue
          line =  '%12d\t'   % event.PIX_rdoID[i]
          line += '%12.5f\t' % np.sqrt(event.PIX_position_x[i]**2+event.PIX_position_y[i]**2)
          line += '%12.5f\t' % np.arctan2(event.PIX_position_y[i],event.PIX_position_x[i])
          line += '%12.5f\t' % event.PIX_position_z[i]
          line += '%12d\t' % ((event.PIX_layerDisk[i]+1)*2)
          line += '%12d\t' % (event.PIX_barrelEndcap[i]/2+8)
          line += '%12d\t'   % event.PIX_trk_barcode[i]
          line += '%12.5f\n' % event.PIX_trk_pt[i]
          csvfile.write(line)
        for i in range(event.SCT_rdoID.size()):
          line =  '%12d\t'   % event.SCT_rdoID[i]
          line += '%12.5f\t' % np.sqrt(event.SCT_position_x[i]**2+event.SCT_position_y[i]**2)
          line += '%12.5f\t' % np.arctan2(event.SCT_position_y[i],event.SCT_position_x[i])
          line += '%12.5f\t' % event.SCT_position_z[i]
          line += '%12d\t' % ((event.SCT_layerDisk[i]+1)*2)
          line += '%12d\t' % (event.SCT_barrelEndcap[i]/2+13)
          line += '%12d\t'   % event.SCT_trk_barcode[i]
          line += '%12.5f\n' % event.SCT_trk_pt[i]
          csvfile.write(line)
      if idx%100==0:
        logger.info('done processing event %s',idx)

    logger.info('done processing event in: %s',file)

if __name__ == "__main__":
    main()
